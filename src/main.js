// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import store from "./store/index";
import router from "./router";
import VueI18n from 'vue-i18n';
import VueCookies from 'vue-cookies';
import layer from 'vue2-layer-mobile';
Vue.use(layer)

Vue.use(VueI18n)
Vue.use(VueCookies)
const i18n = new VueI18n({
  // locale: 'en_US',    // 语言标识
  locale: localStorage.getItem('lang')||'zh_CN',   // 通过切换locale的值来实现语言切换
  messages: {
    'zh_CN': require('./assets/lang/zh'),   // 中文语言包
    'en_US': require('./assets/lang/en')    // 英文语言包
  }
})
console.log(i18n);
console.log(localStorage.getItem('lang')+"测试locale"+i18n.locale)

//按需引入vant
import { Tabbar, TabbarItem ,Swipe, SwipeItem,NoticeBar, Popup,Button, Cell, CellGroup ,Icon,List,Row, Col,PullRefresh ,Actionsheet ,Tag ,Collapse,CollapseItem ,
  Tab, Tabs,Field,Dialog,DatetimePicker} from 'vant';

Vue.use(Tabbar).use(TabbarItem).use(Swipe).use(SwipeItem).use(NoticeBar).use(Popup).use(Button).use(Cell).use(CellGroup).use(Icon).use(Row).use(Col).use(PullRefresh)
.use(List).use(Tab).use(Tabs).use(Field).use(Actionsheet ).use(Tag ).use(Collapse).use(CollapseItem ).use(Dialog).use(DatetimePicker)


import "../src/assets/css/common.css"; //公共样式
import "../src/assets/css/syb-common.css"; //公共样式

import App from "./App";
import httpServer from "./http/axios"; //封装ajax请求
import Api from "./http/api"; //接口api
import validator from "./store/validator";
Vue.prototype.$ajax = httpServer;
Vue.prototype.$api = Api;
Vue.prototype.$validator = validator;
Vue.prototype.goBack = ()=>{
  router.go(-1);
}
Vue.prototype.pushPath = (path)=>{
  router.push(path);
}
Vue.config.productionTip = false;

router.beforeEach((to, from, next) => {
  //检查token有没有
  if (to.meta.requiresAuth) {
    if (localStorage.getItem("token")) {
      next();
    } else {
      next({
        path: "/login",
      }); 
    }
  } else {
    next();
    // localStorage.setItem("token",'')
    // store.commit('updateLoginState',false)
  }
  sessionStorage.setItem('lastPage',from.name);
});
/* eslint-disable no-new */
new Vue({
  el: "#app",
  i18n,
  router,
  store,
  components: { App },
  template: "<App/>"
});
