import Vue from 'vue'
import Router from 'vue-router'

const  soonOpen = r => require.ensure( [], () => r (require('@/view/my/soonOpen')))


const  login = r => require.ensure( [], () => r (require('@/view/accunt/login')))
const  register = r => require.ensure( [], () => r (require('@/view/accunt/register')))
const  forgetPassword = r => require.ensure( [], () => r (require('@/view/accunt/forgetPassword')))

const  home = r => require.ensure( [], () => r (require('@/view/home/home')))
const  convertIntegral = r => require.ensure( [], () => r (require('@/view/home/convertIntegral')))
const  revenueRecord = r => require.ensure( [], () => r (require('@/view/home/revenueRecord')))
const  digitAssets = r => require.ensure( [], () => r (require('@/view/home/digitAssets')))
const  notice = r => require.ensure( [], () => r (require('@/view/home/notice')))
const  tradeInfo = r => require.ensure( [], () => r (require('@/view/home/tradeInfo')))




const  my = r => require.ensure( [], () => r (require('@/view/my/my')))
const  modifyPwd = r => require.ensure( [], () => r (require('@/view/my/modifyPwd')))
const  setting = r => require.ensure( [], () => r (require('@/view/my/setting')))
// const  modifyMsg = r => require.ensure( [], () => r (require('@/view/my/modifyMsg')))
const  circleFriendRecruitment = r => require.ensure( [], () => r (require('@/view/my/circleFriendRecruitment')))
const  Exchangecenter = r => require.ensure( [], () => r (require('@/view/my/Exchangecenter')))
const  myFriend = r => require.ensure( [], () => r (require('@/view/my/myFriend')))
const  enableMenber = r => require.ensure( [], () => r (require('@/view/my/enableMenber')))
const  aboutUs = r => require.ensure( [], () => r (require('@/view/my/aboutUs')))
const  myHoney = r => require.ensure( [], () => r (require('@/view/my/myHoney')))
const  message = r => require.ensure( [], () => r (require('@/view/my/message')))
const  UpdataInfo = r => require.ensure( [], () => r (require('@/view/my/UpdataInfo')))
// 新添实名认证
const  Authentication = r => require.ensure( [], () => r (require('@/view/my/Authentication')))
const  ContactInfo = r => require.ensure( [], () => r (require('@/view/my/ContactInfo')))
const  Dynamicmatc = r => require.ensure( [], () => r (require('@/view/my/Dynamicmatc')))
const  MembershipUP = r => require.ensure( [], () => r (require('@/view/my/MembershipUP')))
const  cusPeoInfo = r => require.ensure( [], () => r (require('@/view/my/cusPeoInfo')))
const  entMemberList = r => require.ensure( [], () => r (require('@/view/my/entMemberList')))
// const  ChangeMyInfo = r => require.ensure( [], () => r (require('@/view/my/ChangeMyInfo')))
const  changeAvatar = r => require.ensure( [], () => r (require('@/view/my/changeAvatar')))
const  myTeam = r => require.ensure( [], () => r (require('@/view/my/myTeam')))
const  myWallt = r => require.ensure( [], () => r (require('@/view/my/myWallt')))
const  myRegLsit = r => require.ensure( [], () => r (require('@/view/my/myRegLsit')))



const  mainMall = r => require.ensure( [], () => r (require('@/view/mall/mainMall')))
const  socialContact = r => require.ensure( [], () => r (require('@/view/socialContact/socialContact')))

// 交易模块
const tradIndex = r => require.ensure( [], () => r (require('@/view/Trading/tradIndex')))
const myGsingle = r => require.ensure( [], () => r (require('@/view/Trading/myGsingle')))
const MyTrading = r => require.ensure( [], () => r (require('@/view/Trading/MyTrading')))
// 新增模块 交易记录
const tradingRecord = r => require.ensure( [], () => r (require('@/view/Trading/tradingRecord')))
// 新增交易模块  交易记录
const TradingDetail = r => require.ensure( [], () => r (require('@/view/Trading/TradingDetail')))
// 资产模块
const assetIndex =r => require.ensure( [], () => r (require('@/view/Asset/assetIndex')))
const guc =r => require.ensure( [], () => r (require('@/view/Asset/guc')))
const theTransactions =r => require.ensure( [], () => r (require('@/view/Asset/theTransactions')))
const stockRights =r => require.ensure( [], () => r (require('@/view/Asset/stockRights')))
const gucRights =r => require.ensure( [], () => r (require('@/view/Asset/gucRights')))
const circulate  =r => require.ensure( [], () => r (require('@/view/Asset/circulate')))
const circulateList = r => require.ensure( [], () => r (require('@/view/Asset/circulateList')))
const toGuc = r => require.ensure( [], () => r (require('@/view/Asset/toGuc')))
const Registration =r => require.ensure( [], () => r (require('@/view/Asset/Registration')))
const Activatepoints =r => require.ensure( [], () => r (require('@/view/Asset/Activatepoints')))
const Bonus  =r => require.ensure( [], () => r (require('@/view/Asset/Bonus')))
const BonusList  =r => require.ensure( [], () => r (require('@/view/Asset/BonusList')))
const FormAsset =r => require.ensure( [], () => r (require('@/view/Asset/FormAsset')))
const ActivatepList = r => require.ensure( [], () => r (require('@/view/Asset/ActivatepList')))
const RegistrationList = r => require.ensure( [], () => r (require('@/view/Asset/RegistrationList')))
const myChildAssHome  = r => require.ensure( [], () => r (require('@/view/Asset/myChildAssHome')))
const myChildEXchange = r => require.ensure( [], () => r (require('@/view/Asset/myChildEXchange')))
const AsesAList = r => require.ensure( [], () => r (require('@/view/Asset/AsesAList')))
const regAsesAList = r => require.ensure( [], () => r (require('@/view/Asset/regAsesAList')))
// 子账户中的订单
const Sonbill = r => require.ensure( [], () => r (require('@/view/Asset/Sonbill')))
// 跳转
const myinfo = r => require.ensure( [], () => r (require('@/view/treasure/myinfo')))
// 新增日收入明显
const Day = r => require.ensure( [], () => r (require('@/view/day/day')))
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: home, 
      meta: { requiresAuth: true }
    },
    {
      path: '*',
      name: 'home',
      component: home,
      meta: { requiresAuth: true }
    },

    {
      path: '/soonOpen',
      name: 'soonOpen',
      component: soonOpen,
      meta: { requiresAuth: true }
    },
    {
      path: '/Day',
      name: 'Day',
      component: Day,
      meta: { requiresAuth: true }
    },
    {
      path: '/login',
      name: 'login',
      component: login
    },
    {
      path: '/register/:phone',
      name: 'register',
      component: register
    },
    {
      path: '/forgetPassword',
      name: 'forgetPassword',
      component: forgetPassword
    },

    {
      path: '/convertIntegral',
      name: 'convertIntegral',
      component: convertIntegral,
      meta: { requiresAuth: true }
    },
    {
      path: '/revenueRecord',
      name: 'revenueRecord',
      component: revenueRecord,
      meta: { requiresAuth: true }
    },
    {
      path: '/digitAssets',
      name: 'digitAssets',
      component: digitAssets,
      meta: { requiresAuth: true }
    },
    {
      path: '/notice',
      name: 'notice',
      component: notice,
      meta: { requiresAuth: true }
    },
    
    {
      path: '/tradeInfo/:type',
      name: 'tradeInfo',
      component: tradeInfo,
      meta: { requiresAuth: true }
    },
    {
      path: '/my',
      name: 'my',      
      component:my,
      meta: { requiresAuth: true }
    },
    {
      path: '/circleFriendRecruitment',
      name:'circleFriendRecruitment',
      component:circleFriendRecruitment,
      meta: { requiresAuth: true }
    },
    {
      path: '/myFriend',
      name:'myFriend',
      component:myFriend,
      meta: { requiresAuth: true }
    },
    {
      path: '/enableMenber',
      name:'enableMenber',
      component:enableMenber,
      meta: { requiresAuth: true }
    },
    {
      path: '/aboutUs',
      name:'aboutUs',
      component:aboutUs,
      meta: { requiresAuth: true }
    },
    
    {
      path: '/Exchangecenter',
      name:'Exchangecenter',
      component:Exchangecenter,
      meta: { requiresAuth: true }
    },
    {
      path: '/myHoney',
      name:'myHoney',
      component:myHoney,
      meta: { requiresAuth: true }
    },
    {
      path:"/modifyPwd/:isLoginPwd",
      component:modifyPwd,
      meta: { requiresAuth: true }
    },
    {
      path:"/setting",
      component:setting,
      meta: { requiresAuth: true }
    },
    {
      path:"/message",
      name:"message",
      component:message,
      meta: { requiresAuth: true }
    },

    {
      path:'/mainMall',
      name:'mainMall',
      component:mainMall,
      meta: { requiresAuth: true } 
    },
    {
      path:'/UpdataInfo',
      name:'UpdataInfo',
      component:UpdataInfo,
      meta: { requiresAuth: true } 
    },
    {
      path:'/Authentication',
      name:'Authentication',
      component:Authentication,
      meta: { requiresAuth: true } 
    },
    {
      path:'/ContactInfo',
      name:'ContactInfo',
      component:ContactInfo,
      meta: { requiresAuth: true } 
    },
    {
      path:'/Dynamicmatc',
      name:'Dynamicmatc',
      component:Dynamicmatc,
      meta: { requiresAuth: true } 
    },
    {
      path:'/MembershipUP',
      name:'MembershipUP',
      component:MembershipUP,
      meta: { requiresAuth: true } 
    },
    {
      path:"/cusPeoInfo",
      name:"cusPeoInfo",
      component:cusPeoInfo,
       meta: { requiresAuth: true }
    },
    {
      path:"/circulateList",
      name:"circulateList",
      component:circulateList,
      meta: { requiresAuth: true }
    },
    {
      path:"/toGuc",
      name:"toGuc",
      component:toGuc,
      meta: { requiresAuth: true }
    },
    {
      path:"/ActivatepList",
      name:"ActivatepList",
      component:ActivatepList,
      meta: { requiresAuth: true }
    },
    {
      path:"/RegistrationList",
      name:"RegistrationList",
      component:RegistrationList,
      meta: { requiresAuth: true }
    },
    {
      path:"/changeAvatar",
      name:"changeAvatar",
      component:changeAvatar,
      meta: { requiresAuth: true }
    },
    {
      path:"/myTeam",
      name:"myTeam",
      component:myTeam,
      meta: { requiresAuth: true }
    },
    {
      path:"/myWallt",
      name:"myWallt",
      component:myWallt,
      meta: { requiresAuth: true }
    },
    {
      path:"/myRegLsit",
      name:"myRegLsit",
      component:myRegLsit,
      meta: { requiresAuth: true }
    },
    // 子账单
    {
      path:"/Sonbill",
      name:"Sonbill",
      component:Sonbill,
      meta: { requiresAuth: true }
    },




    {
      path:"/guc",
      name:"guc",
      component:guc,
      meta: { requiresAuth: true }
    },
    // {
    //   path:"/sellShopInfo",
    //   name:"sellShopInfo",
    //   component:sellShopInfo,
    //   meta: { requiresAuth: true }
    // },
    // {
    //   path:"/payment",
    //   name:"payment",
    //   component:payment,
    //   meta: { requiresAuth: true }
    // },
    // {
    //   path:"/scanCodePayment/:id",
    //   name:"scanCodePayment",
    //   component:scanCodePayment,
    //   meta: { requiresAuth: true }
    // },
    //社交
    {
      path:"/socialContact",
      name:"socialContact",
      component:socialContact,
      meta: { requiresAuth: true }
    },
    // 交易模块
    {
      path:'/tradIndex',
      name:'tradIndex',
      component:tradIndex,
      meta: { requiresAuth: true }      
    },
    {
      path:'/myGsingle',
      name:'myGsingle',
      component:myGsingle,
      meta: { requiresAuth: true }      
    },
    {
      path:'/MyTrading',
      name:'MyTrading',
      component:MyTrading,
      meta: { requiresAuth: true }      
    },
    // 交易记录
    {
      path:'/tradingRecord',
      name:'tradingRecord',
      component:tradingRecord,
      meta: { requiresAuth: true }      
    },
    {
      path:'/TradingDetail',
      name:'TradingDetail',
      component:TradingDetail,
      meta: { requiresAuth: true }      
    },
    // 资产模块 
    {
      path:'/assetIndex',
      name:'assetIndex',
      component:assetIndex,
      meta: { requiresAuth: true }      
    },
    {
      path:'/stockRights',
      name:'stockRights',
      component:stockRights,
      meta: { requiresAuth: true }      
    },
    {
      path:'/gucRights',
      name:'gucRights',
      component:gucRights,
      meta: { requiresAuth: true }      
    },
    // 新增搜索模块
    {
      path:'/theTransactions',
      name:'theTransactions',
      component:theTransactions,
      meta: { requiresAuth: true }      
    },
    {
      path:'/circulate',
      name:'circulate',
      component:circulate,
      meta: { requiresAuth: true }      
    },
    {
      path:'/Registration',
      name:'Registration',
      component:Registration,
      meta: { requiresAuth: true }      
    },
    {
      path:'/Activatepoints',
      name:'Activatepoints',
      component:Activatepoints,
      meta: { requiresAuth: true }      
    },
    {
      path:'/Bonus',
      name:'Bonus',
      component:Bonus,
      meta: { requiresAuth: true }      
    },
    {
      path:'/FormAsset',
      name:'FormAsset',
      component:FormAsset,
      meta: { requiresAuth: true }      
    },
    {
      path:'/BonusList',
      name:'BonusList',
      component:BonusList,
      meta: { requiresAuth: true }      
    },
    {
      path: '/myinfo',
      name:'myinfo',
      component:myinfo,   
    },
    {
      path: '/myChildAssHome/:id',
      name:'myChildAssHome',
      component:myChildAssHome, 
      meta: { requiresAuth: true }     
    },
    {
      path: '/myChildEXchange',
      name:'myChildEXchange',
      component:myChildEXchange, 
      meta: { requiresAuth: true }     
    },
    {
      path: '/AsesAList',
      name:'AsesAList',
      component:AsesAList, 
      meta: { requiresAuth: true }     
    },
    {
      path: '/regAsesAList',
      name:'regAsesAList',
      component:regAsesAList, 
      meta: { requiresAuth: true }     
    },
    {
      path:'/fenxiangList',
      name:'fenxiangList',
      component:circulate,
      meta: { requiresAuth: true }      
    },
    {
      path:'/entMemberList',
      name:'entMemberList',
      component:entMemberList,
      meta: { requiresAuth: true }      
    },
  ]
})
