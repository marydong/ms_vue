import axios from "axios";
import qs from "qs";
import router from "../router/index";
import { Indicator } from "mint-ui";
import { Toast } from "vant";
import api from "./api";

const baseURL = api.baseURL;

axios.interceptors.request.use(
  config => {
    if(config.showLoading){
      // setTimeout(()=>{
        Indicator.open({
          spinnerType: "fading-circle"  
        });
      // },200)
    }
    return config;
  },
  error => {
    return Promise.reject(error);
  }
);

axios.interceptors.response.use(
  response => {
    return response;
  },
  error => {
    return Promise.resolve(error.response);
  } 
);

function errorState(response) {
  // setTimeout(()=>{
    Indicator.close();
  // },200)
  // 如果http状态码正常，则直接返回数据
  if (
    response &&
    (response.status === 200 ||
      response.status === 304 ||
      response.status === 400||
      response.status === 500)
  ) {
    Toast({
      message: "网络不给力",
      position: "bottom"
    });
    
    return response.data;
  } else {
    Toast({
      message: "网络不给力",
      position: "bottom"
    });
    router.push({
      path: "/login",
    // query: { redirect: routers.history.current.path }
    });
  }
}

function successState(response) {
  // setTimeout(()=>{
    Indicator.close();
  // },200)
  //统一判断后端返回的错误码
  
  if (response.data.code == "0") {
    
  } else if (response.data.status == "1") {
    Toast({
      message: response.msg || "网络不给力",
      position: "bottom"
    });
  } else if (response.data.status == "401") {
    Toast({
      message:  "登录信息已失效！",
      position: "bottom",
    });
    if(router.history.current.path!='/login'){
      router.push({
        path: "/login",
      // query: { redirect: routers.history.current.path }
      });
    }
    
  }
  if(response.data.status == "501"){
    Toast({message: "股权配送中",position: "bottom"});
    router.push({
      path: "/login",
    // query: { redirect: routers.history.current.path }
    });
  }else if(response.data.status == "502"){
    Toast({message: "系统维护中",position: "bottom"});
    router.push({
      path: "/login",
    // query: { redirect: routers.history.current.path }
    });
  }
}

const httpServer = (opts, data) => {
  let Public = {
    //公共参数
    token: localStorage.getItem("token"),
    lang: localStorage.getItem("lang")
  };

  let httpDefaultOpts = {
    //http默认配置
    method: opts.method,
    baseURL: baseURL,

    url: opts.url,
    timeout: 60000,
    params: Object.assign(Public, data),
    data: qs.stringify(Object.assign(Public, data)),
    headers:
      opts.method == "get"
        ? {
            "X-Requested-With": "XMLHttpRequest",
            Accept: "application/json",
            "Content-Type": "application/json; charset=UTF-8"
          }
        : {
            "X-Requested-With": "XMLHttpRequest",
            "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
          }
  };
  if (opts.method == "get") {
    delete httpDefaultOpts.data;
  } else {
    delete httpDefaultOpts.params;
  }
  httpDefaultOpts.showLoading = !(data&&data.showLoading=='0')

  let promise = new Promise(function(resolve, reject) {
    axios(httpDefaultOpts)
      .then(res => {
        successState(res);
        resolve(res.data);
      })
      .catch(response => {
        errorState(response);
        reject(response);
      });
  });
  return promise;
};

export default httpServer;
