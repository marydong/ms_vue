const serviceModule = {
  baseURL: process.env.NODE_ENV === 'production' 
  ? "http://admin.msfzdz.cn/"
  : "http://admin.msfzdz.cn/",
  // baseURL: process.env.NODE_ENV === 'production' 
  // ? "http://admin.msfzdz.cn/"
  // : "http://localhost:8080/",
  // 二维码路径
  qrUrl: 'https://www.msfzdz.cn/#/',
  //登录
   // 获取金额总量
   getNum: {
    url: 'api/customer/groupProfit',
    method: 'post'
  },
  login: {
    //url: 'api/common/login',
    url: 'newapi/common/login',
    method: 'post'
  },
  //发送验证码
  getSmsCode: {
    url: 'api/common/sendCode',
    method: 'post'
  },
  //注册
  register: {
    url: 'api/common/registerUser',
    method: 'post'
  },
  //忘记密码
  forgetPass: {
    url: 'api/common/forgetPass',
    method: 'post'
  },
  // 获取国家列表
  getCountry: {
    url: 'api/customer/getCountry', 
    method: 'post'
  },
  //验证token是否失效
  checkToken: {
    url: 'api/common/checkTokenExist.do',
    method: 'post'
  },
  // 修改用户头像
  modifyCustomerHead: {
    url: 'api/common/modifyCustomerHead',
    method: 'post'
  },
  //激活分1 注册分2
  selectDirectTrade: {
    url: 'api/coin/selectDirectTrade',
    method: 'post'
  },  
  //可用股权出售列表
  selectStockList: {
    url: 'api/coin/selectStockList',
    method: 'post'
  },  

  //首页数据

  // 修改发送验证码
  sendCode:{
    url: 'api/common/sendCode',
    method: 'post'
  },
  sendMail:{
    url: 'api/common/sendMail',
    method: 'post'
  },
  //修改登录密码
  modifyLoginPass: {
    url: "api/common/modifyLoginPass",
    method: "post"
  },
  //修改交易密码
  changePayPwd: {
    url: "api/common/modifyPayPass",
    method: "post"
  },
  //检查是否已经注册
  checkLogin: {
    url: "api/common/checkLogin.do",
    method: "post"
  },

  // 上传图片 
  uploadImage: {
    url: 'api/common/uploadImage',
    method: 'post'
  },
  // 上传多张图片 
  uploadImages: {
    url: 'api/common/uploadImages',
    method: 'post'
  },
  // base64文件上传 
  uploadImagesByBase64: {
    url: 'api/common/uploadImagesByBase64',
    method: 'post'
  },
  //  customer-api-controller
  // 用户激活
  actCustomer: {
    url: 'api/customer/actCustomer',
    method: 'post'
  },
  // 兑换中心申请
  applyExchange: {
    url: 'api/customer/applyExchange',
    method: 'post'
  },
  // 意见反馈列表
  getFeedbackList: {
    url: 'api/customer/getFeedbackList',
    method: 'post'
  },
  // 意见反馈类型
  getFeedbackType: {
    url: 'api/customer/getFeedbackType',
    method: 'post'
  },
  // 系统等级
  getLevels: {
    url: 'api/customer/getLevels',
    method: 'post'
  },
  // 修改个人信息
  modifyCustomer: {
    url: 'api/customer/modifyCustomer',
    method: 'post'
  },
  // 提出反馈
  pushFeedback: {
    url: 'api/customer/pushFeedback',
    method: 'post'
  },
  // 动态配套复投
  reinvest: {
    url: 'api/customer/reinvest',
    method: 'post'
  },
  // 个人账户查询
  selectAccount: {
    //url: 'api/customer/selectAccount',
    url: 'newapi/customer/selectAccount',
    method: 'post'
  },
  // 查询banner
  selectBanner: {
    url: "api/customer/selectBanner",
    method: "post"
  },
  //我的 个人信息查询
  getMyInfo: {
    //url: 'api/customer/selectCustomer',
    url: 'newapi/customer/selectCustomer',

    method: 'post'
  },
  // 我的粉丝
  selectFans: {
    url: 'api/customer/selectFans',
    method: 'post'
  },
  // 公告
  selectNotice: {
    url: 'api/customer/selectNotice',
    method: 'post'
  },
  // 收益记录
  selectProfit: {
    url: 'api/customer/selectProfit',
    method: 'post'
  },

   // guc收益记录
   selectGucProfit: {
    url: 'api/customer/selectGucProfit',
    method: 'post'
  },
  // 种子账户收益记录
  selectChildProfit: {
      url: 'api/child/selectProfit',
      method: 'post'
  },
  // 在线升级
  upgrade: {
    url: 'api/customer/upgrade',
    method: 'post'
  },

  // trade-api-controller
  // 取消交易
  cancelTrade: {
    url: 'api/trade/cancelTrade',
    method: 'post'
  },
  // 验证是否能进入交易大厅
  checkTrade: {
    url: 'api/trade/checkTrade',
    method: 'post'
  },
  // 确定交易
  confirmTrade: {
    url: 'api/trade/confirmTrade',
    method: 'post'
  },
  // 匹配交易
  matchTrade: {
    url: 'api/trade/matchTrade',
    method: 'post'
  },
  // 发布交易信息
  pushTrade: {
    url: 'api/trade/pushTrade',
    method: 'post'
  },

  //流通股转guc的ms积分
  usepointGuc: {
    url: 'api/trade/usepointGuc',
    method: 'post'
  },
  // 上传交易凭证
  pushTradeImg: {
    url: 'api/trade/pushTradeImg',
    method: 'post'
  },
  // 今日市值
  selectMarket: {
    url: 'api/trade/selectMarket',
    method: 'post'
  },
  // 我的交易记录列表(买入/卖出)
  selectMyTrade: {
    url: 'api/trade/selectMyTrade',
    method: 'post'
  },
  // 我的交易记录列表(type:1是待匹配,2是交易中) 
  selectMyTrading: {
    url: 'api/trade/selectMyTrading',
    method: 'post'
  },
  // 交易大厅记录查询
  selectTradeList: {
    url: 'api/trade/selectTradeList',
    method: 'post'
  },
  // 交易量和每日成交额
  selectTradeTable: {
    url: 'api/trade/selectTradeTable',
    method: 'post'
  },
  // 历史交易记录查询
  selectTradingRecord: {
    url: 'api/trade/TradingRecord',
    method: 'post'
  },
  // 1214
  // 购买可用股权
  buyStock:{
    url:"api/coin/buyStock",
    method:"post"
  },
  // 资产转换
  convertCoin:{
    url:"api/coin/convertCoin",
    method:"post"
  },
  // 定向交易
  directTrade:{
    url:"api/coin/directTrade",
    method:"post"
  },
  // 出售可用股权
  saleStock:{
    url:"api/coin/saleStock",
    method:"post"
  },
   // 提现guc
   withdrawGuc:{
    url:"api/coin/withdrawGuc",
    method:"post"
  },
  // 资产转换记录
  selectConvert:{
    url:"api/coin/selectConvert",
    method:"post"
  },
  // 定向交易记录
  selectDirectTrade:{
    url:"api/coin/selectDirectTrade",
    method:"post"
  },
  // 可用股权出售列表
  selectStockList:{
    url:"api/coin/selectStockList",
    method:"post"
  },

  // 可用股权出售列表
  selectMyStockList:{
    url:"api/coin/selectMyStockList",
    method:"post"
  },
  generateChild:{
    url:"api/child/generateChild",
    method:"post"
  },
  childAccount:{
    //url:"api/child/childAccount",
    url:"newapi/child/childAccount",
    method:"post"
  },
  childList:{
    //url:"api/child/childList",
    url:"newapi/child/childList",
    method:"post"
  },
  selectMyMembers:{
    url:"api/customer/selectMyMembers",
    method:"post"
  },
  getCenter:{
    url:"api/customer/getCenter",
    method:"post"
  },
  selectMessage:{
    url:"api/customer/selectMessage",
    method:"post"
  },
  selectChild:{
    url:"api/child/selectChild",
    method:"post"
  },
  transferToAccount:{
    url:"api/child/transferToAccount",
    method:"post"
  },
  selectExchange:{
    url:"api/trade/selectExchange",
    method:"post"
  },
  selectInfoByAccount :{
    url:"api/customer/selectInfoByAccount ",
    method:"post"
  },
  // 我的小蜜
  getFeedbackType:{
    url: 'api/customer/getFeedbackType',
    method: 'post'
  },
  pushFeedback:{
    url: 'api/customer/pushFeedback',
    method: 'post'
  },
  getFeedbackList:{
    url: 'api/customer/getFeedbackList',
    method: 'post'
  },
}
const Api = { ...serviceModule }

export default Api